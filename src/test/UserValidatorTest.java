package test;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import form.User;
import validator.UserValidator;

/**
 * UserValidatorテスト
 * <pre>
 * ■課題クリア条件■
 * １：validator.UserValidatorについて、メソッドごとのテストコードを記載しカバレッジが100%になること。
 *     （すべてのif条件がテストされていること）
 * ２：入力チェックの正常系2パターンが網羅されていること。
 * ３：メッセージ内容の確認がされていること。
 * ４：エラーメッセージの件数が確認されていること。</pre>
 *
 * @see validator.UserValidator
 */
public class UserValidatorTest {

	/**
	 * ※これはテストのサンプルコードです。
	 * 正常異常系
	 * すべて未入力の場合
	 * 条件
	 *   すべて未入力
	 *   新規作成フラグtrue
	 * 期待値
	 *   エラーメッセージ件数：7件
	 */
	@Test
	public void accountNothingTest() {
		List<String> resultMsgList = new ArrayList<>();
		User user = new User();

		resultMsgList = UserValidator.chkUserData(user, true);
		assertThat(6, is(resultMsgList.size()));
	}

	/*****************************************************
	 * ↓ここから実装
	 *****************************************************/

}
