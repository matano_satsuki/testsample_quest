package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import dao.BranchDao;
import form.Branch;

public class BranchService {

	/**
	 * 支店リスト取得
	 * @param account
	 * @return ユーザ数
	 */
	public List<Branch> getBranchList() {

        Connection connection = null;
        try {
            connection = getConnection();

            BranchDao branchDao = new BranchDao();
            List<Branch> branchList = branchDao.getBranchList(connection);

            commit(connection);

            return branchList;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}
