package service;

import static utils.CloseableUtil.*;
import static utils.DBUtil.*;

import java.sql.Connection;

import dao.UserDao;
import form.User;
import utils.CipherUtil;

public class UserService {

	/**
	 * ユーザ登録
	 * @param user
	 */
	public void register(User user) {

        Connection connection = null;
        try {
            connection = getConnection();

            String encPassword = CipherUtil.encrypt(user.getPassword());
            user.setPassword(encPassword);

            UserDao userDao = new UserDao();
            userDao.insert(connection, user);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

	/**
	 * ユーザ数チェック
	 * @param account
	 * @return ユーザ数
	 */
	public int chkUser(String account) {

        Connection connection = null;
        try {
            connection = getConnection();

            UserDao userDao = new UserDao();
            int userCnt = userDao.chkUser(connection, account);

            commit(connection);

            return userCnt;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}
