package controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mysql.jdbc.StringUtils;

import form.Branch;
import form.User;
import service.BranchService;
import service.UserService;
import validator.UserValidator;

/**
 * ユーザ新規登録画面
 */
@WebServlet(urlPatterns = {"/signup"})
public class SignUp extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		HttpSession session = request.getSession();
		List<Branch> branchList =  new BranchService().getBranchList();
		session.setAttribute("branchList", branchList);
		request.getRequestDispatcher("signup.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		User user = new User();
		user.setName(request.getParameter("name"));
		user.setAccount(request.getParameter("account"));
		user.setPassword(request.getParameter("password"));
		user.setChk_password(request.getParameter("chk_password"));
		user.setBranch_id(parseInt(request.getParameter("branch_id")));
		user.setPosition_id(parseInt(request.getParameter("position_id")));

		HttpSession session = request.getSession();

		// エラー判定
		List<String> errMsgs = UserValidator.chkUserData(user, true);

		UserService userSv= new UserService();

		// ユーザ存在チェック
		if (userSv.chkUser(user.getAccount()) > 0) {
			errMsgs.add("既に存在するアカウントIDです。異なるアカウントIDを使用してください。");
		}

		if (errMsgs.size() > 0) {
			// エラーメッセージの中身がある場合、チェックエラーのため新規登録画面へ再度遷移
			session.setAttribute("errMsgs", errMsgs);
			request.getRequestDispatcher("/webPJ/signup?id=1").forward(request, response);
		} else {
			// 登録処理
			userSv.register(user);

			// HOMEへ戻る
			response.sendRedirect("./");
		}
	}

	private int parseInt(String strValue) throws IOException{
		int intValue = 0;
		if (!StringUtils.isNullOrEmpty(strValue)) {
			Integer.parseInt(strValue);
		}
		return intValue;
	}
}
